

DESCRIPTION:
  Present statistics of the site on the URL /userstats
  in both tabular and graphical form
. Number of user registrations, comments, likes and 
    new ideas (content type) per month.

  Charts are generated with libchart
  http://naku.dohcrew.com/libchart
  http://naku.dohcrew.com/libchart/pages/samplecode/
  libcard is found by the libraries module, i.e. normally in sites/all/libraries

INSTALLATION
. install libraries
. install libchart in sites/all/libraries/   (libchart/classes/libchart.php)
. install & enable this module
. mkdir sites/MYSITE/files/sitegraphs
. visit /userstats and check the watchdog log for errors

This module is a sandbox, since its still site specific, but the code may be of interest to others.

by Sean Boran, boran@drupal.org


TO DO
- make it more generic...
- make content types/titles variables (this module was develop for types idea/news)
  Started, site variables:
  sitegraphs_ct1 default='idea'
  sitegraphs_ct2 default='news'
- test uninstall: files/dir being deleted properly?

